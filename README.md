# node-tdd [![CircleCI](https://circleci.com/bb/bsato212/node-tdd/tree/master.svg?style=svg)](https://circleci.com/bb/bsato212/node-tdd/tree/master) [![Codacy Badge](https://api.codacy.com/project/badge/Grade/8e21c7630ff343c5a6b9e67d4d813454)](https://www.codacy.com/app/brunoborsato/node-tdd?utm_source=bsato212@bitbucket.org&amp;utm_medium=referral&amp;utm_content=bsato212/node-tdd&amp;utm_campaign=Badge_Grade) [![Codacy Badge](https://api.codacy.com/project/badge/Coverage/8e21c7630ff343c5a6b9e67d4d813454)](https://www.codacy.com/app/brunoborsato/node-tdd?utm_source=bsato212@bitbucket.org&utm_medium=referral&utm_content=bsato212/node-tdd&utm_campaign=Badge_Coverage)

## NVM

### Install
```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
```

### Upgrade
```
(
  cd "$NVM_DIR"
  git fetch --tags origin
  git checkout `git describe --abbrev=0 --tags --match "v[0-9]*" $(git rev-list --tags --max-count=1)`
) && \. "$NVM_DIR/nvm.sh"
```

### Node.js Install
```
nvm ls-remote --lts
nvm install <version>
nvm alias default <version>
```

---

## Yarn

### Install/Upgrade
```
curl -o- -L https://yarnpkg.com/install.sh | bash
```

---

## Yarn Scripts

### Install Packages
```
yarn install
```

### Run ESLint
```
yarn lint
```

### Run Tests
```
yarn test
```

### Start Local Server
```
yarn start
yarn dev
```

---

## Visual Studio Code

### Install
```
https://code.visualstudio.com/docs/setup/mac
https://code.visualstudio.com/docs/setup/linux
```

### ESLint Extension
```
https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint
```

---

## Catching Errors with ESLint

### Add Package as Dev Dependency
```
yarn add eslint --dev
```

### Add Script to package.json
```
"scripts": {
  "lint": "eslint ."
},
```

### Rule Definition
```
.eslintrc
https://eslint.org/docs/rules/
```

### Find Errors
```
yarn lint
./node_modules/.bin/eslint <path>
```

### Fix Errors
```
yarn lint --fix
./node_modules/.bin/eslint <path> --fix
```

---

## Unit Test Setup

### Add Package as Dev Dependency
```
yarn add jest --dev
```

### Add Script to package.json
```
"scripts": {
  "test": "jest --coverage"
},
```

### Write Unit Tests
```
<file>.test.js
https://jestjs.io/docs/en/getting-started
```

### Run Tests
```
yarn test
./node_modules/.bin/jest
```

---

## Run Cloud Functions Locally
- Cloud Functions are based on Express 4.16.3;
- Allows you to export a handler, Router or Express app with middleware;
- With a bootstrap file it is possible to run exported module locally: index.js;

---

## Debug
- Select index.js;
- Debug > Add Configuration;
- Add Breakpoint;
- Debug > Start Debugging;

---
