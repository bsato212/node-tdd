const log = require('loglevel');
const settings = require('./settings');

log.setLevel(settings.logLevel);

module.exports = log;
