const axios = require('axios');
const get = require('lodash/get');
const log = require('./logger');

function getUrl(user) {
  if (!user) {
    return null;
  }

  return `https://api.github.com/users/${user}`;
}

function fetchUserData(user) {
  const url = getUrl(user);

  return axios.get(url)
    .then(response => {
      const data = get(response, 'data', null);
      if (!data) {
        return null;
      }

      const { login, name, location } = data;

      return {
        login,
        name,
        location,
      };
    })
    .catch(error => {
      log.error(error);
      return null;
    });
}

module.exports = {
  fetchUserData,
};
