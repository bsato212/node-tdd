/* eslint-env jest */

const request = require('supertest');
const axios = require('axios');
const { app } = require('./app');

jest.mock('./logger');
jest.mock('axios');

describe('App routes', () => {
  test('invalid params', async () => {
    axios.get.mockResolvedValue({
      data: {
        login: 'foo',
        name: 'bar',
        location: 'baz',
      }
    });

    const response = await request(app).get('/');

    expect(response.statusCode).toBe(400);
  });

  test('invalid API response', async () => {
    axios.get.mockResolvedValue({
      data: null,
    });

    const response = await request(app)
      .get('/')
      .query({
        user: 'foo',
      });

    expect(response.statusCode).toBe(500);
  });

  test('valid API response', async () => {
    axios.get.mockResolvedValue({
      data: {
        login: 'foo',
        name: 'bar',
        location: 'baz',
      }
    });

    const response = await request(app)
      .get('/')
      .query({
        user: 'foo',
      });

    expect(response.statusCode).toBe(200);
  });

  test('check headers', async () => {
    axios.get.mockResolvedValue({
      data: {
        login: 'foo',
        name: 'bar',
        location: 'baz',
      }
    });

    const response = await request(app)
      .get('/')
      .query({
        user: 'foo',
      })
      .set('Accept', 'application/json');

    expect(response.statusCode).toBe(200);
    expect(response.headers['content-type']).toMatch(/json/u);
    expect(response.headers['access-control-allow-origin']).toBeDefined();
    expect(response.headers['x-xss-protection']).toBeDefined();
    expect(response.headers['x-content-type-options']).toBeDefined();
    expect(response.headers['strict-transport-security']).toBeDefined();
    expect(response.headers['x-frame-options']).toBeDefined();
    expect(response.headers['x-dns-prefetch-control']).toBeDefined();
    expect(response.headers['x-download-options']).toBeDefined();
    expect(response.headers['x-powered-by']).toBeUndefined();
  });
});
