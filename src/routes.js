const { Router } = require('express');
const get = require('lodash/get');
const log = require('./logger');
const GitHub = require('./github');

const router = new Router();

router.get('/', async (req, res) => {
  const user = get(req, 'query.user', null);
  if (!user) {
    return res.status(400).json({
      error: 'Invalid user',
    });
  }

  log.info(`user = [${user}]`);

  const userData = await GitHub.fetchUserData(user);
  if (!userData) {
    return res.status(500).json({
      error: 'Failed to fetch user data',
    });
  }

  return res.json({
    error: false,
    ...userData,
  });
});

module.exports = router;
