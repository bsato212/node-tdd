/* eslint-env jest */

const rewire = require('rewire');
const axios = require('axios');
const GitHub = require('./github');

jest.mock('./logger');
jest.mock('axios');

describe('GitHub module', () => {
  test('getUrl returns null on invalid user', () => {
    const rewireGitHub = rewire('./github');
    const getUrl = rewireGitHub.__get__('getUrl');

    expect(getUrl(null)).toBeNull();
  });

  test('fetchUserData returns null on invalid user', async () => {
    axios.get.mockRejectedValue(null);

    const user = null;
    const userData = await GitHub.fetchUserData(user);
    expect(userData).toBeNull();
  });

  test('fetchUserData returns null on API error', async () => {
    axios.get.mockRejectedValue(null);

    const user = 'foo';
    const userData = await GitHub.fetchUserData(user);
    expect(userData).toBeNull();
  });

  test('fetchUserData returns null on API invalid response', async () => {
    axios.get.mockResolvedValue({
      data: null,
    });

    const user = 'foo';
    const userData = await GitHub.fetchUserData(user);
    expect(userData).toBeNull();
  });

  test('fetchUserData returns valid response on API success', async () => {
    axios.get.mockResolvedValue({
      data: {
        login: 'foo',
        name: 'bar',
        location: 'baz',
      },
    });

    const user = 'foo';
    const userData = await GitHub.fetchUserData(user);
    expect(userData).toBeDefined();
    expect(userData.login).toEqual(user);
  });
});
